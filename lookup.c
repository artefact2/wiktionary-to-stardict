/* Author: Romain "Artefact2" Dal Maso <artefact2@gmail.com> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <arpa/inet.h>
#include "common.h"

int main(int argc, char** argv) {
	if(argc != 3) {
		fprintf(stderr, "Usage: %s <ifo-file> <word>\n", argv[0]);
		return 1;
	}

	size_t pathlen = strlen(argv[1]);
	if(pathlen < 4 || strcmp(".ifo", argv[1] + pathlen - 4)) {
		fprintf(stderr, "%s: path to ifo file dosen't look sane\n", argv[0]);
		return 1;
	}

	FILE* ifo = fopen(argv[1], "rb");
	if(ifo == NULL) {
		perror("cannot open ifo file");
		return 1;
	}

	char magic[25];
	if(!fread(magic, 25, 1, ifo)) {
		perror("fread failed");
		return 1;
	}
	if(strncmp("StarDict's dict ifo file\n", magic, 25)) {
		fprintf(stderr, "%s: wrong ifo magic\n", argv[0]);
		return 1;
	}

	int wordcount = -1, idxfilesize = -1;
	char line[32];
	char* intend;
	while((wordcount == -1 || idxfilesize == -1) && !feof(ifo)) {
		fgets(line, 32, ifo);

		for(size_t i = 0; i < 31; ++i) {
			if(line[i] == '\n') break;
			if(i == 30) {
				/* long line, keep reading until newline */
				while(!feof(ifo) && fgetc(ifo) != '\n');
			}
		}

		if(!strncmp("wordcount=", line, 10)) {
			wordcount = strtol(line + 10, &intend, 10);
		} else if(!strncmp("idxfilesize=", line, 12)) {
			idxfilesize = strtol(line + 12, &intend, 10);
		} else {
			continue;
		}

		if(*intend != '\n' && *intend != '\0') {
			fprintf(stderr, "%s: cannot parse number in line: %s", argv[0], line);
			return 1;
		}
	}

	if(wordcount == -1 || idxfilesize == -1) {
		fprintf(stderr, "%s: ifo file is missing wordcount or idxfilesize\n", argv[0]);
		return 1;
	}

	FILE* idx;
	int idxlen;
	/* Can't think of a better way of doing this */
	argv[1][pathlen - 2] = 'd';
	argv[1][pathlen - 1] = 'x';
	idx = fopen(argv[1], "rb");
	if(idx == NULL) {
		perror("cannot open idx file");
		fprintf(stderr, "%s: decompress foo.idx.gz with gunzip -k foo.idx.gz\n", argv[0]);
		return 1;
	}

	if(fseek(idx, 0, SEEK_END) < 0) {
		perror("fseek failed");
		return 1;
	}

	if((idxlen = ftell(idx)) < 0) {
		perror("ftell failed");
		return 1;
	}

	if(idxlen != idxfilesize) {
		fprintf(stderr, "%s: inconsistent idxfilesize: %d (ifo) =/= %d (actual)\n", argv[0], idxfilesize, idxlen);
		return 1;
	}

	char* idxmap;
	idxmap = mmap(NULL, idxlen, PROT_READ, MAP_SHARED, fileno(idx), 0);
	if(idxmap == MAP_FAILED) {
		perror("cannot mmap idx file");
		return 1;
	}

	const void** idxdata;
	size_t datalen;
	parse_index(idxmap, idxlen, &idxdata, &datalen);

	if(datalen != wordcount) {
		fprintf(stderr, "%s: inconsistent wordcount: %d (ifo) =/= %zu (actual)\n", argv[0], wordcount, datalen);
		return 1;
	}

	/* Check index consistency */
	for(size_t i = 1; i < datalen; ++i) {
		if(stardict_compare(idxdata[i-1], idxdata[i]) >= 0) {
			fprintf(stderr, "%s: inconsistent idx ordering (%s >= %s)\n", argv[0], idxdata[i-1], idxdata[i]);
			return 1;
		}
	}

	/* Very simple search through a sorted array, I'm sure it can be improved. */
	size_t start = 0, end = datalen - 1, mid;
	int c;
	while(start < end) {
		mid = end - (end - start) / 2;
		c = stardict_compare(argv[2], idxdata[mid]);

		if(c == 0) break;
		else if(c < 0) {
			end = mid - 1;
		} else {
			start = mid + 1;
		}
	}

	if(strcmp(idxdata[mid], argv[2])) {
		fprintf(stderr, "%s: query %s not found in dictionary\n", argv[0], argv[2]);
		return 2;
	}

	size_t wordlen = strlen(idxdata[mid]);
	uint32_t doffset;
	int32_t dlength;

	memcpy(&doffset, idxdata[mid] + wordlen + 1, 4);
	memcpy(&dlength, idxdata[mid] + wordlen + 5, 4);
	doffset = ntohl(doffset);
	dlength = ntohl(dlength);

	fprintf(stderr, "%s: found %s at offset %d for %d bytes\n", argv[0], argv[2], doffset, dlength);

	/* XXX really? */
	char dictpath[pathlen + 2];
	memcpy(dictpath, argv[1], pathlen - 3);
	dictpath[pathlen - 3] = 'd';
	dictpath[pathlen - 2] = 'i';
	dictpath[pathlen - 1] = 'c';
	dictpath[pathlen + 0] = 't';
	dictpath[pathlen + 1] = '\0';

	FILE* dict = fopen(dictpath, "rb");
	if(dict == NULL) {
		perror("cannot open dict file");
		fprintf(stderr, "%s: decompress foo.dict.dz with gunzip -k -S .dz foo.dict.dz\n", argv[0]);
		return 1;
	}

	if(fseek(dict, doffset, SEEK_SET) < 0) {
		perror("could not seek in dict file");
		return 1;
	}

	char type;
	while(dlength) {
		if(dlength < 0) {
			fprintf(stderr, "%s: read more data than necessary, broken idx file?\n", argv[0]);
			return 1;
		}

		switch((type = fgetc(dict))) {
		case 'm':
			while((type = fgetc(dict))) {
				--dlength;
				putchar(type);
			}
			dlength -= 2;
			break;

		case EOF:
			fprintf(stderr, "%s: unexpected EOF, still have %d bytes to read, broken idx file?\n", argv[0], dlength);
			return 1;

		default:
			fprintf(stderr, "%s: don't know what do do with entry type %c in dict\n", argv[0], type);
			return 1;
		}
	}

	/* XXX: no cleanup is done on error */
	fclose(dict);
	munmap(idxmap, idxlen);
	fclose(idx);
	fclose(ifo);
	return 0;
}
