<?php
/* Author: Romain "Artefact2" Dal Maso <artefact2@gmail.com> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

function prettify(string $title, string $html, array $config): ?string {
	$doc = new \DOMDocument('1.0', 'utf-8');
	if($doc->loadXML('<root>'.$html.'</root>') === false) {
		fprintf(STDERR, "%s: page %s has unparseable content\n", __FUNCTION__, $title);
		return null;
	}
	$xp = new \DOMXPath($doc);

	/* Prune languages & TOC */
	$keep = false;
	foreach($xp->query('//div[@class=\'mw-parser-output\']/h2') as $h2) {
		if(preg_match('%^'.$config['languages'].'($|\[)%', $h2->textContent)) {
			$keep = true;
			continue;
		}

		$node = $h2;
		do {
			$next = $node->nextSibling;
			$node->parentNode->removeChild($node);
			$node = $next;
		} while($node !== null && $node->nodeName !== 'h2');
	}
	if(!$keep) {
		if($xp->query('//div[@class=\'redirectMsg\']')->length === 0) {
			return null;
		}
	}

	/* Prune sections */
	$stopat = '';
	for($i = 2; $i <= 6; ++$i) {
		$tag = 'h'.$i;
		$stopat .= '|'.$tag;
		foreach($xp->query('//div[@class=\'mw-parser-output\']/'.$tag) as $heading) {
			if(preg_match('%^('.$config['languages'].'|'.$config['sections'].')($|\[)%', $heading->textContent)) {
				continue;
			}

			$node = $heading;
			do {
				$next = $node->nextSibling;
				$node->parentNode->removeChild($node);
				$node = $next;
			} while($node !== null && !preg_match('%^('.$stopat.')$%', $node->nodeName));
		}
	}

	if(!$config['tableofcontents']) {
		/* Prune TOC */
		$divs = $xp->query('//div[@id=\'toc\']');
		assert($divs->length <= 1);
		foreach($divs as $d) $d->parentNode->removeChild($d);
	}

	/* Prune translation tables */
	foreach($xp->query('//table[@class=\'translations\']//li') as $li) {
		if(preg_match('%^'.$config['languages'].':%', $li->textContent)) continue;
		$li->parentNode->removeChild($li);
	}

	/* "Trim" */
	foreach($xp->query('//li|//dl|//dd') as $li) {
		while($li->firstChild !== null && $li->firstChild->nodeType == \XML_TEXT_NODE && preg_match('%^\s*$%', $li->firstChild->wholeText)) $li->removeChild($li->firstChild);
		while($li->lastChild !== null && $li->lastChild->nodeType == \XML_TEXT_NODE && preg_match('%^\s*$%', $li->lastChild->wholeText)) $li->removeChild($li->lastChild);
	}

	/* Prune this (incl children) */
	foreach($xp->query('//span[@class=\'mw-editsection\']|//img|//sup|//table[@class=\'audiotable\']|//span[@class=\'tpos\']/a[@class=\'extiw\']|//div[@class=\'thumbinner\']|//br') as $e) {
		$e->parentNode->removeChild($e);
	}

	/* Prune this (but keep children) */
	foreach($xp->query('//span|//div|//a|//b|//i|//strong|//em|//abbr|//table[@class=\'translations\']|//table[@class=\'translations\']/tr|//table[@class=\'translations\']/tr/td|//dl|//dd|//cite') as $e) {
		if($e->nodeName === 'div' && $e->hasAttribute('class') && preg_match('%(^|\s)sister-project($|\s)%', $e->getAttribute('class'))) {
			$e->parentNode->removeChild($e);
			continue;
		}

		while($e->hasChildNodes()) $e->parentNode->insertBefore($e->childNodes->item(0), $e);
		$e->parentNode->removeChild($e);
	}

	/* Prune this, but only if "empty" */
	foreach($xp->query('//li|//ul|//ol') as $e) {
		if(!$e->hasChildNodes() || preg_match('%^\s*$%', $e->textContent)) {
			$e->parentNode->removeChild($e);
		}
	}

	/* Formatting */
	foreach($xp->query('//p') as $p) {
		while($p->hasChildNodes()) $p->parentNode->insertBefore($p->childNodes->item(0), $p);
		$p->parentNode->removeChild($p);
	}
	foreach($xp->query('//h2|//h3|//h4|//h5|//h6') as $h) {
		while($h->nextSibling !== null && $h->nextSibling->nodeType === \XML_TEXT_NODE && preg_match('%^\s*$%', $h->nextSibling->wholeText)) {
			$h->parentNode->removeChild($h->nextSibling);
		}
		$h->parentNode->replaceChild($doc->createTextNode("\n\n".strtoupper($h->textContent)."\n"), $h); /* XXX: rtrim */
	}

	/* Lists */
	foreach($xp->query('//ul|//ol') as $ul) {
		for($i = 0; $i < $ul->childNodes->length; ++$i) {
			$li = $ul->childNodes->item($i);

			if($li->nodeType === XML_TEXT_NODE) {
				assert(preg_match('%^\s*$%', $li->textContent));
				$ul->removeChild($li);
				--$i;
				continue;
			} else assert($li->nodeName === 'li');
		}
	}
	foreach($xp->query('//ul/li|//ol/li') as $li) {
		if($li->parentNode->nodeName === 'ul') {
			$prefix = '—';
		} else {
			$prefix = '';

			$pli = $li;
			while($pli !== null) {
				/* XXX really inefficient */
				for($i = 0; $i < $pli->parentNode->childNodes->length; ++$i) {
					if($pli->isSameNode($pli->parentNode->childNodes->item($i))) {
						$prefix = ($i+1).'.'.$prefix;
						do {
							$pli = $pli->parentNode;
						} while ($pli !== null && $pli->nodeName !== 'li');
						break;
					}
				}
			}
		}

		$li->insertBefore($doc->createTextNode(' '.$prefix.' '), $li->firstChild);
		$li->appendChild($doc->createTextNode("\n"));
	}
	foreach($xp->query('//ul|//ol|//li') as $ul) {
		while($ul->hasChildNodes()) $ul->parentNode->insertBefore($ul->childNodes->item(0), $ul);
		$ul->parentNode->removeChild($ul);
	}


	/* XXX: Tables */
	foreach($xp->query('//table') as $table) {
		$table->parentNode->replaceChild($doc->createTextNode('(a table is supposed to be here)'), $table);
	}

	/* XXX: can't we do it properly with DOM? */
	/* Collapse newlines */
	$plain = preg_replace('%\n\n+%', "\n", $doc->documentElement->textContent);

	/* Proper \n\n before headings */
	$plain = preg_replace('%^([A-Z][A-Z0-9 ]+)$%m', "\n\$1", $plain);

	/* Inline some short content after titles */
	$plain = preg_replace('%^([A-Z][A-Z0-9 ]+)\n([^\n]{0,30})\n\n%m', "\$1: $2\n\n", $plain);

	return trim($plain)."\n";
}
