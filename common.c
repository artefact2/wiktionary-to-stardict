/* Author: Romain "Artefact2" Dal Maso <artefact2@gmail.com> */

/* This program is free software. It comes without any warranty, to the
 * extent permitted by applicable law. You can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details. */

#include "common.h"

void parse_index(const char* data, size_t len, const void*** out, size_t* outlen) {
	size_t offset, i;
	*outlen = 0;
	for(offset = 0; offset < len; offset += strlen(&(data[offset])) + 9) ++(*outlen);
	*out = malloc(*outlen * sizeof(void*));
	if(*out == NULL) {
		perror("malloc failed");
		exit(1);
	}

	for(offset = 0, i = 0; offset < len; offset += strlen(&(data[offset])) + 9) {
		(*out)[i++] = &(data[offset]);
	}
}

static char ascii_lower(char a) {
	return (a >= 'A' && a <= 'Z') ? (a - 'A' + 'a') : a;
}

int stardict_compare(const char* a, const char* b) {
	/* Case-insensitive strcmp, but only for A-Z letters */
	size_t i = 0;

	while(a[i] != '\0' && ascii_lower(a[i]) == ascii_lower(b[i])) ++i;
	if(a[i] == '\0' && b[i] == '\0') return strcmp(a, b);
	return (unsigned char)ascii_lower(a[i]) - (unsigned char)ascii_lower(b[i]);
}
