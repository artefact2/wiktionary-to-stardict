default:
	@echo "Please see README.md for instructions."

# XXX
dict:
	make out/enwiktionary.dict
	make out/enwiktionary.ifo

out/%.ifo: out/%.dict out/%.idx sort-idx
	./sort-idx out/$*.idx
	./write-ifo out/$*
	dictzip -fk out/$*.dict
	gzip -9fk out/$*.idx

common.o: common.c
	clang -std=c11 -Wall -O2 -c -o $@ $<

sort-idx: sort-idx.c common.o
	clang -std=c11 -Wall -D_POSIX_C_SOURCE=199309L -O2 -o $@ $^

lookup: lookup.c common.o
	clang -std=c11 -Wall -O2 -D_POSIX_C_SOURCE -o $@ $^

out/%.dict: fetched/enwiktionary-latest-all-titles-in-ns0.gz
	rm -f out/$*.{dict,idx}
	touch $@
	find fetched -name "*.json.xz" | parallel -Xn 100 -d '\n' ./write-dict-and-idx out/$* | pv -ls `zcat $< | tail -n+2 | wc -l` >/dev/null

fetch: fetched/enwiktionary-latest-all-titles-in-ns0.gz fetch-words
	zcat $< | tail -n+2 | xargs -d '\n' -n 100 -P 8 ./fetch-words | pv -ls `zcat $< | tail -n+2 | wc -l` >/dev/null

fetched/enwiktionary-latest-all-titles-in-ns0.gz:
	cd fetched && wget -O {,'https://dumps.wikimedia.org/enwiktionary/latest/'}enwiktionary-latest-all-titles-in-ns0.gz

dict-clean:
	find out -mindepth 1 -not -name ".empty_file" -delete

fetch-clean:
	find fetched -mindepth 1 -not -name ".empty_file" -delete

.PHONY: default dict fetch dict-clean fetch-clean
.PRECIOUS: fetched/enwiktionary-latest-all-titles-in-ns0.gz
